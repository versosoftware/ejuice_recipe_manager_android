package mehdipour.matin.eliquiddiary;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by matt on 4/28/15.
 */
public class EliquDBHelper extends SQLiteOpenHelper
{
    Context c;
    private static final String DB_NAME = "eqliq_db";
    private static final String TABLE_NAME = "eliq_table";
    private static final String id = "_id";
    private static final String RECIPE_NAME = "recipe_name";
    private static final String FLAVOR_NAME = "flavor_name";

    private final String createTable = "CREATE TABLE " + TABLE_NAME + "(" + id + " integer primary key" +
            ", " + RECIPE_NAME + " text, " + FLAVOR_NAME + " text);";


    public EliquDBHelper(Context context)
    {
        super(context, DB_NAME, null, 1);
    }

    public void add()
    {
        ContentValues cv = new ContentValues();

        cv.put(RECIPE_NAME, "some recipe");
        cv.put(FLAVOR_NAME, "some flavor");

        SQLiteDatabase db = getWritableDatabase();

        db.insert(TABLE_NAME, null, cv);


        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
}
